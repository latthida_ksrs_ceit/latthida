int analogValue = 0;
int ledValue = 0;

void setup() {
  Serial.begin(9600);
  pinMode(11, OUTPUT);
  
}

void loop() {
  analogValue = analogRead(A0);
  //Serial.println(analogValue);

  ledValue = map(analogValue, 0, 1023, 0, 255);
  //Serial.println(ledValue);
  
  analogWrite(11, ledValue);
  delay(10);
}
