const int redPin = 11;
const int greenPin = 10;
const int bluePin = 9;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

}

void loop() {
  
  Serial.print("RGB LED ON: ");
  Serial.println("White Color");
  onLED(redPin, 0);
  onLED(greenPin, 0);
  onLED(bluePin, 0);
  delay(1000);
  
  Serial.println("OFF");
  offLED(redPin, 0);
  offLED(greenPin, 0);
  offLED(bluePin, 0);
  delay(1000);
  
  Serial.print("RGB LED ON: ");
  Serial.println("Red Color");
  redLight(1000);
  
  Serial.print("RGB LED ON: ");
  Serial.println("Green Color");
  greenLight(1000);
  
  Serial.print("RGB LED ON: ");
  Serial.println("Blue Color");
  blueLight(1000);
  
}

void onLED(int pin, int time){
  digitalWrite(pin, HIGH);
  delay(time);
}
void offLED(int pin, int time){
  digitalWrite(pin, LOW);
  delay(time);
}

void redLight(int time){
  onLED(redPin, time);
  Serial.println("OFF");
  offLED(redPin, time);
}

void greenLight(int time){
  onLED(greenPin, time);
  Serial.println("OFF");
  offLED(greenPin, time);
}

void blueLight(int time){
  onLED(bluePin, time);
  Serial.println("OFF");
  offLED(bluePin, time);
}